let icon = true;

function displyMenuBar(){
    let openMenuEl = document.getElementById("openMenu");
    let ulItemsEl = document.getElementById("ulItems");
    let mobileImgEl = document.getElementById("mobileImg");
    
    if(icon === true){
        openMenuEl.src = "./images/icon-close.svg"
        ulItemsEl.classList.remove("hide-item");
        mobileImgEl.style.display = "none"
        icon = false
    }else{
        openMenuEl.src = "./images/icon-hamburger.svg"
        ulItemsEl.classList.add("hide-item");
        mobileImgEl.style.display = "block"
        icon = true
    }
};